<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/products', 'ProductsController@getProducts');
Route::get('/product/{id}', 'ProductsController@getProductById');
Route::get('/productDelete/{id}', 'ProductsController@deleteProduct');
Route::post("/update","ProductsController@updateProduct")->name("update");
Route::post("/addImage","ProductsController@addImage")->name("addImage");
Route::post("/add","ProductsController@addProduct")->name("add");
Route::post('/gallery', 'GalleryController@addGallery');
Route::get('/galleries', 'GalleryController@getGallery');
Route::get('/galleryDelete/{id}', 'GalleryController@deleteGallery');
Route::post('/login', 'LoginController@login');


