<?php

namespace App\Http\Controllers;

use App\Gallery;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class GalleryController extends Controller
{
    public function getGallery() {
        $gallery = Gallery::all();
        return response()->json($gallery);
    }
    public function addGallery(Request $request) {
        $data = $request->all();
        $gallery = Gallery::create([
            'img' => $data['img']
        ]);
    }
    public function deleteGallery ($id) {
        $gallery = Gallery::where("id",$id)->first();
        Storage::delete('public/'.$gallery->img);
        $gallery->delete();
    }
}
