<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class ProductsController extends Controller
{
    public function getProducts ()
    {
        $products = Product::all();
        return response()->json($products);
    }

    public function getProductById ($id)
    {
        $product = Product::where("id", $id)->first();
        return response()->json($product);
    }

    public function updateProduct (Request $request)
    {
        try{
            DB::beginTransaction();
            $data = $request->data;
            $product = Product::where("id",$data['id'])->first();
            $oldImage = $product->img;
            $product->name = $data['name'];
            $product->description = $data['description'];
            $product->price = $data['price'];
            $product->img = $data['img'];
            $product->save();
            if (array_key_exists('img', $data)){
                Storage::delete('public/'.$oldImage);
            }
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            return $exception->getMessage();
        }

    }

    public function deleteProduct ($id) {
        $product = Product::where("id",$id)->first();
        Storage::delete('public/'.$product->img);
        $product->delete();
    }

    public function addProduct (Request $request)
    {
        $data = $request->all();
        $product = Product::create([
            'name' => $data['name'],
            'price' => $data['price'],
            'description' => $data['description'],
            'img' => $data['img'],
        ]);
    }

    public function addImage (Request $request) {
        $storage = Storage::disk('public')->put('images/products',$request->file);
        return $storage;
    }
}
