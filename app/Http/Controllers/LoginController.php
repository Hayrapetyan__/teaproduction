<?php

namespace App\Http\Controllers;

use App\User;
use function Couchbase\defaultDecoder;
use Illuminate\Http\Request;
use Validator;


class LoginController extends Controller
{
    public function login(Request $request)
    {
        $data = $request->data;
        $user = User::where("login",$data['username'])->first();
        if(!$user){
            return 'Մուտքանունը կամ Գախտնաբառը սխալ է';
        } else {
            if($user['password'] == $data['password']){
                return ;
            } else {
                return "Մուտքանունը կամ Գախտնաբառը սխալ է";
            }
        }
    }
}
