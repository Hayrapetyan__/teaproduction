<?php
/**
 * Created by PhpStorm.
 * User: kar
 * Date: 11/15/18
 * Time: 12:08 PM
 */

namespace App\Http\Repositories;

use App\Http\Services\UsersInterface;
use App\User;


class UsersRepository implements UsersInterface
{
    protected $userRepo;

    /**
     * UsersRepository constructor.
     * @param UsersInterface $userRepo
     */
    public function __construct(User $user)
    {
        $this->userRepo = $user;
    }

    /**
     * User's create
     *
     * @param $data
     * @return mixed
     */
    public function create($data)
    {
        return $this->userRepo->create($data);
    }

    /**
     * Get user by id
     *
     * @param $id
     * @return mixed
     */
    public function getById($id)
    {
        return $this->userRepo->where('id',$id)->findOrFail();
    }
    
}