<?php
/**
 * Created by PhpStorm.
 * User: kar
 * Date: 11/15/18
 * Time: 5:50 PM
 */

namespace App\Http\Services;


interface UsersInterface
{
    /**
     * User's create
     *
     * @param $data
     * @return mixed
     */
    public function create($data);

    /**
     * Get user by id
     *
     * @param $id
     * @return mixed
     */
    public function getById($id);
}