<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'product';
    protected $fillable = ['name', 'description', 'price', 'img'];
    public function Category()
    {
        return $this->belongsTo(Category::class);

    }
}
