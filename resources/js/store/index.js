import Vue from "vue"
import Vuex from "vuex"

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        globalCart:{},
    },
    getters:{
        getProducts: state => state.globalCart
    },
    mutations:{
        addProducts(state, payload){
            state.globalCart = payload
        }
    }
})