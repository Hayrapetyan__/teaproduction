import homePage from "../components/HomeComponent.vue"
import aboutPage from "../components/AboutComponent.vue"
import CategoryPage from "../components/CategoryComponent.vue"
import adminPage from "../components/AdminComponent.vue"
import productPage from "../components/ProductComponent.vue"
import GalleryPage from "../components/GalleryComponent.vue"
import LoginPage from "../components/LoginComponent.vue"

const routes = [
    {
        path: '/',
        name: 'Գլխավոր',
        component: homePage
    },
    {
        path: '/About',
        name: 'Մեր մասին',
        component: aboutPage
    },
    {
        path: '/Category',
        name: 'Տեսականի',
        component: CategoryPage
    },
    {
        path: '/admin',
        name: 'admin',
        component: adminPage
    },
    {
        path: '/login',
        name: 'login',
        component: LoginPage
    },
    {
        path: '/product/:id',
        name: 'Ապրանքանիշ',
        component: productPage,
        props:true
    },
    {
        path: '/gallery',
        name: 'Տեսադարան',
        component: GalleryPage
    }
];

export default routes;