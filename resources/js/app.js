
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
import Vue from 'vue';
import BootstrapVue from "bootstrap-vue"
import "bootstrap/dist/css/bootstrap.min.css"
import "bootstrap-vue/dist/bootstrap-vue.css"
import vue2Dropzone from 'vue2-dropzone'
import 'vue2-dropzone/dist/vue2Dropzone.min.css'
import "animate.css/animate.css"
import router from './router'
import store from './store'
import App from './components/App.vue'
import header from './components/HeaderComponent.vue'
import breadcrumbs from "./components/BreadcrumbsComponent.vue"
import footer from './components/FooterComponent.vue'
import VuePaginate from 'vue-paginate'


require('./bootstrap');

Vue.use(BootstrapVue);
Vue.use(vue2Dropzone);
Vue.use(VuePaginate);


/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

Vue.component('header-navbar', header);
Vue.component('footer-navbar', footer);
Vue.component('app-breadcrumbs', breadcrumbs);
Vue.component('vueDropzone', vue2Dropzone);
//todo: vue-router
// const files = require.context('./', true, /\.vue$/i)

// files.keys().map(key => {
//     return Vue.component(_.last(key.split('/')).split('.')[0], files(key))
// })

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
const app = new Vue({
    el: '#app',
    router,
    store,
    render: h => h(App)
});

